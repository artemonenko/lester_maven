package lester_maven.pages;

        import java.io.File;
        import java.io.IOException;
        import java.util.Random;
        import java.util.concurrent.TimeUnit;

        import org.apache.commons.io.FileUtils;
        import org.openqa.selenium.By;
        import org.openqa.selenium.OutputType;
        import org.openqa.selenium.TakesScreenshot;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.firefox.FirefoxDriver;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;
        import org.testng.Assert;
        import org.testng.annotations.AfterClass;
        import org.testng.annotations.BeforeClass;
        import org.testng.annotations.Test;


/*0661864246 - Саша
0955789631 - Антон
0507125525 - Вика
0951702079 - Аня
0663033934 - Ира
0508246084 - Dasha
0937320128 - Sony
0937878742 -Ira Life*/



public class Lester{

    String linkpage ="http://lester.ua/195-r14s-hankook-winter-rw-06-106104q/p26713";
    String phoneNumber = "0500000000";
    String personalPhoneNumber = "0508246084"; //Даша
    //String phoneNumberRand;
    String email ="test@test.ua";
    String login = "0661864246";
    String password= "12345678";

    public WebDriver driver;
    public WebDriverWait wait;



    @BeforeClass
    public void setup(){
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 25);
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        driver.navigate().to("http://lester.ua/");
        driver.manage().deleteAllCookies();
    }


    @AfterClass
    public void teardown() throws InterruptedException{
        Thread.sleep(180000);
        driver.quit();
    }




    @Test(priority =1)
    public void OpeningBrowser() throws InterruptedException, IOException{
        driver.get("http://lester.ua/");
        Assert.assertEquals(driver.getTitle(), "Купить летние, зимние, всесезонные шины (автошины) на автомобиль в Киеве и всей Украине. Новая летняя и зимняя резина (авторезина) - цены в Киеве и всей Украине");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")));
        //Thread.sleep(2000);
        //	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        //FileUtils.copyFile(scrFile, new File("d:\\screenshot.png"));

    }

    @Test(priority =2)
    public void logIn() throws InterruptedException{
        driver.findElement(By.xpath("//*[text()='Войти в кабинет']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='enterRegPopup zoomAnim']")));
        driver.findElement(By.xpath("//*[text()='Войти']")).click(); // костыль :)
        driver.findElement(By.xpath(".//*[@data-name='login']")).sendKeys(login);
        driver.findElement(By.xpath(".//*[@data-name='password']")).sendKeys(password);
        driver.findElement(By.xpath("//*[text()='Войти']")).click();
        Thread.sleep(2000);
        System.out.println("OpeningBrowser done");
    }





    @Test(priority =3)
    public void MakeCallBack() throws InterruptedException{

        System.out.println("Заказ звонка");
        driver.findElement(By.xpath("//*[text()='Заказать обратный звонок']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='backPhone']")));
        driver.findElement(By.xpath("//*[text()='Позвоните мне']")).click();
        driver.findElement(By.xpath(".//*[@id='backPhone']")).sendKeys(personalPhoneNumber);
        driver.findElement(By.xpath("//*[text()='Позвоните мне']")).click();

        driver.findElement(By.xpath("//*[text()='Закрыть']")).click();
        Thread.sleep(3000);
        System.out.println("Быстрый заказ");

    }



    @Test(priority =4)
    public void MakeQuickOrder() throws InterruptedException {
        driver.get(linkpage);
        //driver.navigate().refresh();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Быстрый заказ']")));
        driver.findElement(By.xpath("//*[text()='Быстрый заказ']")).click();
        driver.findElement(By.xpath("//*[@class='tac']//button")).click();
        driver.findElement(By.xpath(".//*[@id='backPhone']")).sendKeys(phoneNumber);
        driver.findElement(By.xpath("//*[@class='tac']//button")).click();

        System.out.println("Стандартный заказ");
        driver.navigate().refresh();
        Thread.sleep(4000);


    }


    @Test(priority =5)
    public void MakeOrder() throws InterruptedException  {

        driver.get(linkpage);
        driver.findElement(By.cssSelector(".btn.btnGreen.buyBtn.mfiB.microTranslate")).click();
        System.out.println("Кликаем по Оформить заказ");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@href='/cart']")));
        driver.findElement(By.xpath("//*[@href='/cart']")).click();

        driver.findElement(By.xpath(".//*[@id='n_fio']")).clear();
        driver.findElement(By.xpath(".//*[@id='n_fio']")).sendKeys("Тестовый Заказ");
        driver.findElement(By.xpath("//*[text()='Продолжить оформление']")).click();

        driver.findElement(By.xpath(".//*[@id='n_email']")).clear();
        driver.findElement(By.xpath(".//*[@id='n_email']")).sendKeys(email);
        driver.findElement(By.cssSelector(".wSubmit.btn.btnCorner.btnGreen")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@for='sam'][@class='dItemMainLabel']")));
        driver.findElement(By.xpath(".//*[@for='sam'][@class='dItemMainLabel']")).click();
        driver.findElement(By.xpath("//*[text()='Далее']")).click();

        driver.findElement(By.xpath(".//*[@id='comment']")).sendKeys(" **** Это Тестовый заказ! Реагировать не нужно! **** ");
        driver.findElement(By.xpath("//*[text()='Оформить заказ']")).click();
        Thread.sleep(3000);
        System.out.println("Тесты завершены");

    }





}
